﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;
namespace lab1_regex
{
    // (abc+)*|(abc)+
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader sr = new StreamReader(@"C:\Users\markp\Desktop\volsu\6_semester\processes\lab1_regex\data\raw.txt");
            string rawData = sr.ReadLine();
            sr.Close();

            if (rawData.Length <= 0) { 
                Console.WriteLine("Нет входных данных");
                return;
            }

            // OriginalRegex(rawData);
            StreamWriter sw = new StreamWriter(@"C:\Users\markp\Desktop\volsu\6_semester\processes\lab1_regex\data\result.txt");
            sw.Flush();
            sw.Write(CustomRegex(rawData));
            sw.Close();
        }

        static string CustomRegex(string rawData) {
            string resultRow = rawData;

            int startPos = 0;

            while (startPos < rawData.Length - 1) {
                int endMatchPos = getLongestMatchEndPos(startPos, resultRow);

                if (startPos != endMatchPos) {
                    resultRow = resultRow.Remove(startPos, endMatchPos - startPos + 1);
                    endMatchPos -= (endMatchPos - startPos);
                }

                startPos = endMatchPos + 1;
            }


            return resultRow;
        }

        static int getLongestMatchEndPos(int startMatchPosition, string rawData) {
            int endMatchPosition = startMatchPosition;
            int strCursor = startMatchPosition;
            
            if (strCursor < rawData.Length - 2 && rawData[strCursor] == 'a' && rawData[strCursor + 1] == 'b' && rawData[strCursor + 2] == 'c') {
                endMatchPosition = strCursor;
                strCursor += 2;
                endMatchPosition = strCursor;
                while (strCursor < rawData.Length - 2 && rawData[strCursor + 1] == 'c') {
                    strCursor++;
                    endMatchPosition = strCursor;
                }
                if (strCursor < rawData.Length - 3 && rawData[strCursor + 1] == 'a' && rawData[strCursor + 2] == 'b' && rawData[strCursor + 3] == 'c') {
                    endMatchPosition = getLongestMatchEndPos(endMatchPosition + 1, rawData);
                }
            }
            
            return endMatchPosition;
        }

        static List<string> OriginalRegex(string rawData)
        {
            Regex regex = new Regex("(abc+)*|(abc)+");

            List<string> allMathes = new List<string>();
            foreach (Match match in regex.Matches(rawData))
            {
                if (match.Value != "")
                {
                    Console.WriteLine(match);
                }
            }

            return allMathes;
        }
    }
}
